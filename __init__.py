# -*- coding: utf-8 -*-
"""
/***************************************************************************
 raster2postgis
                                 A QGIS plugin
 Import raster files to a postgis database
                             -------------------
        begin                : 2019-09-20
        copyright            : (C) 2019 by Geomaster
        email                : geral@geomaster.pt
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load Raster2postgis class from file raster2postgis.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .raster2postgis import Raster2postgis
    return Raster2postgis(iface)
