## Developement

The following steps can be used to setup the project for testing and developing:

### Development dependencies

The following software is needed to develop and build the puglin
```
apt install qttools5-dev-tools
```

This plugin __requires postgis and gdal__ to be installed in the database server in order to import a raster
```
apt install postgis
apt install gdal
```

### Virtualenv

Setup your python environment
```
apt install python-virtualenv

virtualenv -p python3 env
source env/bin/activate
pip3 install psycopg2
pip3 install pyqt5
pip3 install unidecode
```

### Edit Qt dialogs

Use Qt designer (or Qt Creator) to edit the .ui files
```
designer -qt5 "<path to .ui file>"
```

### Preview Qt dialogs
You can run the following command in order to preview ui dialogs
```
pyuic5 -d -p "<path to .ui file>"
```

### Make and package

```
make compile
make zip
```

### Deploy

Deploy and test in your local QGIS 3 installation
```
make deploy
```
Open plugin manager in QGIS, then search for this plugin and enable it.

## TODO

Enabling postgis rasters using Grand Unified Scheme (GUC) variables.

```sql
SHOW postgis.enable_outdb_rasters;
 postgis.enable_outdb_rasters 
------------------------------
 on
(1 row)
```

```sql
SET postgis.gdal_enabled_drivers = 'ENABLE_ALL';
SET postgis.gdal_enabled_drivers = 'DISABLE_ALL';

SHOW postgis.gdal_enabled_drivers;
 postgis.gdal_enabled_drivers 
------------------------------
 ENABLE_ALL
(1 row)
```

Show the supported drivers on the server:

```sql
SELECT short_name  FROM ST_GDALDrivers();
```

## Create fresh test clusters

```sql
sudo systemctl stop postgresql@13-main
```

```sql
pg_dropcluster --stop 13 aux
pg_createcluster -p 5432 13 aux
echo "listen_addresses = '*'" >> /etc/postgresql/13/aux/postgresql.conf
echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/13/aux/pg_hba.conf
pg_ctlcluster 13 aux start
psql
CREATE ROLE qgis LOGIN SUPERUSER PASSWORD '20201231';
CREATE DATABASE qgis OWNER qgis;
\c qgis
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
```

```sql
pg_dropcluster --stop 13 qgis

pg_createcluster -p 5432 13 qgis
echo "listen_addresses = '*'" >> /etc/postgresql/13/qgis/postgresql.conf
echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/13/qgis/pg_hba.conf
pg_ctlcluster 13 qgis start
psql
CREATE ROLE qgis LOGIN SUPERUSER PASSWORD '20201231';
CREATE DATABASE qgis OWNER qgis;
\c qgis
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_raster;
```

